package fr.univlille.iutinfo.m3105.modelQ2;

import fr.univlille.iutinfo.m3105.modelQ1.ITemperature;
import fr.univlille.iutinfo.m3105.modelTP3.WaitTimer;
import fr.univlille.iutinfo.m3105.utils.ConnectableProperty;

public class Temperature extends ConnectableProperty implements ITemperature {
	private final Echelle ECHELLE;
	private final WaitTimer TIMER = new WaitTimer(5.0, this::resetValue);
	
	public Temperature(Echelle echelle) {
		ECHELLE = echelle;
		resetValue();
		TIMER.start();
	}
	
	private void resetValue() {
		double kelvin = Echelle.CELSIUS.toKelvin(18.0);
		setValue(kelvin);
	}

	public Echelle getEchelle() {
		return ECHELLE;
	}

	@Override
	public void setTemperature(double d) {
		setValue(ECHELLE.toKelvin(d));
		TIMER.resetTimer();
	}

	@Override
	public Double getTemperature() {
		return ECHELLE.fromKelvin((Double)getValue());
	}

	@Override
	public void incrementTemperature() {
		addToTemperature(+1.0);
	}

	@Override
	public void decrementTemperature() {
		addToTemperature(-1.0);
	}
	
	private void addToTemperature(Double x) {
		setTemperature(getTemperature() + x);
	}

	@Override
	public ConnectableProperty temperatureProperty() {
		return this;
	}
}
