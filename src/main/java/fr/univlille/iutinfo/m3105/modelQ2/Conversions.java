package fr.univlille.iutinfo.m3105.modelQ2;

// Formules de https://fr.wikipedia.org/wiki/Temp%C3%A9rature#Conversion
public class Conversions {
	// === KELVIN ===
	
	public static Double kelvinToKelvin(Double kelvin) {
		return kelvin; // :^)
	}
	
	// === CELSIUS ===
	
	public static Double CELSIUS_OFFSET = 273.15;
	
	public static Double kelvinToCelsius(Double kelvin) {
		return kelvin - CELSIUS_OFFSET;
	}
	
	public static Double celsiusToKelvin(Double celsius) {
		return celsius + CELSIUS_OFFSET;
	}
	
	// === FAHRENHEIT ===
	
	public static Double FAHRENHEIT_OFFSET = 459.67;
	public static Double FAHRENHEIT_FACTOR = 9.0 / 5.0;
	
	public static Double kelvinToFahrenheit(Double kelvin) {
		return (kelvin * FAHRENHEIT_FACTOR) - FAHRENHEIT_OFFSET;
	}
	
	public static Double fahrenheitToKelvin(Double fahrenheit) {
		return (fahrenheit + FAHRENHEIT_OFFSET) / FAHRENHEIT_FACTOR;
	}
	
	// === RANKINE ===
	
	public static Double RANKINE_FACTOR = FAHRENHEIT_FACTOR;
	
	public static Double kelvinToRankine(Double kelvin) {
		return RANKINE_FACTOR * kelvin;
	}
	
	public static Double rankineToKelvin(Double rankine) {
		return rankine / RANKINE_FACTOR;
	}
	
	// === REAUMUR ===
	
	public static Double REAUMUR_OFFSET = CELSIUS_OFFSET;
	public static Double REAUMUR_FACTOR = 4.0 / 5.0;
	
	public static Double kelvinToReaumur(Double kelvin) {
		return (kelvin * REAUMUR_FACTOR) - REAUMUR_OFFSET;
	}
	
	public static Double reaumurToKelvin(Double reaumur) {
		return (reaumur + REAUMUR_OFFSET) / REAUMUR_FACTOR;
	}
	
	// === NEWTON ===
	
	public static Double NEWTON_OFFSET = CELSIUS_OFFSET;
	public static Double NEWTON_FACTOR = 100.0 / 33.0;
	
	public static Double kelvinToNewton(Double kelvin) {
		return (kelvin - NEWTON_OFFSET) / NEWTON_FACTOR;
	}
	
	public static Double newtonToKelvin(Double newton) {
		return (newton * NEWTON_FACTOR) + NEWTON_OFFSET;
	}
}
