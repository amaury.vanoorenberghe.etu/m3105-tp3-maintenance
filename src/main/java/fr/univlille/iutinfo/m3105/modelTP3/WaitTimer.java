package fr.univlille.iutinfo.m3105.modelTP3;

public class WaitTimer extends Thread {
	private static final int ONE_SECOND = 1000;
	private final int TRIGGER_AFTER;
	private final Runnable CALLBACK;
	
	private int currentTime = 0;
	private boolean hasRun = false;
	
	private boolean running = true;
	
	public WaitTimer(double seconds, Runnable callback) {
		TRIGGER_AFTER = (int)Math.round(seconds * ONE_SECOND);
		CALLBACK = callback;
	}

	@Override
	public void run() {
		try {
			while (running) {
				if (!hasRun) {
					if (currentTime >= TRIGGER_AFTER) {
						CALLBACK.run();
						stopTimer();
					}
				}
				
				sleep(1, 0);
				++currentTime;
			}			
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
	
	public void resetTimer() {
		currentTime = 0;
		hasRun = false;
	}
	
	public void stopTimer() {
		hasRun = true;
	}
	
	public void resumeTimer() {
		hasRun = false;
	}
	
	public void cancelTimer() {
		running = false;
		interrupt();
	}
}
