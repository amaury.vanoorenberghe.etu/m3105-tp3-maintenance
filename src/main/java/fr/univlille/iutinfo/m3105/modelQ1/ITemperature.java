package fr.univlille.iutinfo.m3105.modelQ1;

import fr.univlille.iutinfo.m3105.utils.ConnectableProperty;

public interface ITemperature {
	public void setTemperature(double d);
	public Double getTemperature();

	public ConnectableProperty temperatureProperty();
	
	public void incrementTemperature();
	public void decrementTemperature();
}