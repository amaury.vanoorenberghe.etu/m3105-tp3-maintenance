package fr.univlille.iutinfo.m3105.modelQ1;

import fr.univlille.iutinfo.m3105.utils.ConnectableProperty;

public class Thermogeekostat implements ITemperature {
	protected final ConnectableProperty tempProp = new ConnectableProperty();

	public Thermogeekostat() {
		temperatureProperty().setValue(0.0);
	}
	
	@Override
	public ConnectableProperty temperatureProperty() {
		return tempProp;
	}
	
	@Override
	public void setTemperature(double d) {
		temperatureProperty().setValue(d);
	}

	@Override
	public Double getTemperature() {
		return (Double)temperatureProperty().getValue();
	}

	@Override
	public void incrementTemperature() {
		addToValue(+1.0);
	}

	@Override
	public void decrementTemperature() {
		addToValue(-1.0);
	}

	
	protected void addToValue(Double x) {
		Double temp = getTemperature();
		temp += x;
		setTemperature(temp);
	}
}