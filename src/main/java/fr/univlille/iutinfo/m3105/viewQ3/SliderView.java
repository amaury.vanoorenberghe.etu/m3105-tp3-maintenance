package fr.univlille.iutinfo.m3105.viewQ3;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;
import fr.univlille.iutinfo.m3105.viewQ1.ITemperatureView;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SliderView extends Stage implements ITemperatureView, Observer {
	private final Temperature MODEL;
	
	private final Slider SLIDER;
	private final Button DECREMENT_BTN, INCREMENT_BTN;
	
	public SliderView(Temperature model) {
		MODEL = model;
		MODEL.temperatureProperty().attach(this);
		
		SLIDER = new Slider(-10000.0, 10000.0, MODEL.getTemperature());
				
		DECREMENT_BTN = new Button("-");
		INCREMENT_BTN = new Button("+");
		
		DECREMENT_BTN.setOnAction((e) -> decrementAction());
		INCREMENT_BTN.setOnAction((e) -> incrementAction());
		
		SLIDER.valueProperty().addListener((e) -> {
			MODEL.setTemperature(SLIDER.getValue());
		});
		
		VBox root = new VBox();
		root.setAlignment(Pos.CENTER);
		root.setSpacing(4);
		
		root.getChildren().addAll(DECREMENT_BTN, SLIDER, INCREMENT_BTN);
		
		Scene scene = new Scene(root, 320, 240);
		
		setScene(scene);
		setResizable(false);
		
		update(MODEL, MODEL.getTemperature());
	}
	
	@Override
	public double getDisplayedValue() {
		return MODEL.getTemperature();
	}

	@Override
	public void incrementAction() {
		MODEL.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		MODEL.decrementTemperature();
	}

	@Override
	public void update(Subject subj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Subject subj, Object data) {
		if (MODEL.temperatureProperty().equals(subj)) {
			Double value = MODEL.getTemperature();
			
			SLIDER.setValue(value);
			
			setTitle(MODEL.getEchelle().getName());
		}
	}
}